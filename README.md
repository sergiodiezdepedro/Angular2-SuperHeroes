# Angular 2 Superhéroes

Este proyecto ha sido generado con [Angular CLI](https://github.com/angular/angular-cli) version 1.1.1.

## Servidor de Desarrollo

Ejecuta `ng serve` para utilizar un servidor de desarrollo. Dirígete a  `http://localhost:4200/`. La app recargará automáticamente si se produce un cambio en cualquiera de los archivos fuente.

## Creación e integración de un componente / otro recurso

Ejecuta `ng generate component component-name` para generar un **componente** nuevo. También se puede usar `ng generate directive|pipe|service|class|module`.

## Generar la app para producción

Ejecuta `ng build` para construir el proyecto. Todos los archivos necesarios se guardarán en el directorio `dist/`. Se puede usar la *flag* `-prod` para una versión para producción.

## Realizar tests unitarios

Ejecuta `ng test` para realizar tests unitarios vía [Karma](https://karma-runner.github.io).

## Realizar tests *end-to-end*

Ejecuta `ng e2e` para iniciar tests *end-to-end* vía [Protractor](http://www.protractortest.org/). Antes de iniciar los tests asegúrate de que la app se está ejecuntando vía `ng serve`.

## Ayuda

Para obtener más ayuda sobre *Angular CLI* puedes ejecutar `ng help` o revisar el [README de Angular CLI](https://github.com/angular/angular-cli/blob/master/README.md).

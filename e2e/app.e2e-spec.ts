import { Angular2SuperHeroesPage } from './app.po';

describe('angular2-super-heroes App', () => {
  let page: Angular2SuperHeroesPage;

  beforeEach(() => {
    page = new Angular2SuperHeroesPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});

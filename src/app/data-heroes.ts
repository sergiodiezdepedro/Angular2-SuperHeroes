import { Heroe } from './heroe';

export const HEROES: Heroe[] = [
  {
    id: 1,
    nombre: 'Wolverine',
    nombre_real: 'James Howlett',
    imagen: 'assets/img/wolverine.jpg',
    imagen2: 'assets/img/wolverine2.jpg',
    descripcion: 'Wolverine fue el tercer mutante conocido en nacer, después de su medio hermano Dientes de Sable y siendo el primero Apocalipsis. Logan es el líder de los X-Men que perdió su memoria por culpa del programa Arma X y desde entonces ha tratado de recuperarla integrando el grupo de los X-Men dirigido por el Profesor Charles Xavier para proteger a la Humanidad de los mutantes malignos.'
  },

   {
    id: 2,
    nombre: 'Captain America',
    nombre_real: 'Steven Rogers',
    imagen: 'assets/img/captain_america.jpg',
    imagen2: 'assets/img/captain_america2.jpg',
    descripcion: 'Steven Rogers es producto de una mutación genética para crear a un supersoldado. Debido a que la prueba fue un éxito, adquirió habilidades fisicas impresionantes y sobrehumanas, tales como resistencia, fuerza, habilidad para pelear. Es un ser humano mejorado en todos los sentidos. Es considerado como el Primer Vengador, y también es el lider de los Vengadores.'
  },

  {
    id: 3,
    nombre: 'Thor',
    nombre_real: 'Thor Odinson',
    imagen: 'assets/img/thor.jpg',
    imagen2: 'assets/img/thor2.jpg',
    descripcion: 'Thor Odinson es un príncipe-guerrero Asgardiano, el Dios del Trueno, y un protector autoproclamado de la Tierra. Tuvo una infancia buena, criado en Asgard como el atesorado hijo de Odín y su esposa Frigga. Su mejor amigo y compañero de juegos era su hermano adoptivo Loki y aunque eran rivales por el trono de su padre, eran compañeros fraternales.'
  },

  {
    id: 4,
    nombre: 'Daredevil',
    nombre_real: 'Matthew Michael Murdock',
    imagen: 'assets/img/daredevil.jpg',
    imagen2: 'assets/img/daredevil2.jpg',
    descripcion: 'Abandonado por su madre, Matt Murdock fue criado por su padre, el boxeador Jack "Battling" Murdock, en Hell\'s Kitchen (barrio de Manhattan, Nueva York) . Al darse cuenta de que las reglas son necesarias para evitar que las personas se comporten indebidamente, el joven Matt decidió estudiar Derecho. Sin embargo, al tratar de impedir un accidente, un camión derramó su carga radioactiva dejando ciego a Matt. Sorprendentemente, la radiación incrementó sus cuatro sentidos restantes.'
  },

  {
    id: 5,
    nombre: 'Spiderman',
    nombre_real: 'Peter Benjamin Parker',
    imagen: 'assets/img/spiderman.jpg',
    imagen2: 'assets/img/spiderman2.jpg',
    descripcion: 'Peter Parker es un huérfano criado por su tío Ben y su tía May. Al ser adolescente, tiene que lidiar con todos los problemas típicos de un joven de su edad además de enfrentarse a los criminales con un disfraz rojo y azul. Entre sus superpoderes se encuentra una gran agilidad y flexibilidad, así como la capacidad de aferrarse a la mayoría de superficies. Además, logró crear unos lanzatelarañas o lanzarredes. Otro de sus superpoderes principales es su sentido arácnido, que le avisa cuando está en peligro o cuando el peligro se acerca.'
  },

   {
    id: 6,
    nombre: 'Scarlet Witch',
    nombre_real: 'Wanda Maximoff',
    imagen: 'assets/img/scarlet_witch.jpg',
    imagen2: 'assets/img/scarlet_witch2.jpg',
    descripcion: 'Después de la trágica muerte de su hija, Anya, en un incendio provocado por una multitud antimutante, el mutante Magneto, utilizó sus poderes y exterminó a toda la multitud ante la mirada horrorizada de su esposa, Magda. Magda huyó de su marido hacia la pequeña Nación de Transia en los Montes Balcanes, estando ya embarazada de mellizos. Magda dio a luz en la Montaña Wundagore, "La ciudadela del Alto Evolucionador", a los mellizos, Wanda y Pietro, conocido como Quicksilver. Ese día el dios Antiguo caído conocido como Chton le infundió a la recién nacida Wanda, un gran potencial mágico. Magda murió luego del parto.'
  },

  {
    id: 7,
    nombre: 'Hulk',
    nombre_real: 'Robert Bruce Banner',
    imagen: 'assets/img/hulk.jpg',
    imagen2: 'assets/img/hulk2.jpg',
    descripcion: 'Hulk es retratado como un ser de forma humanoide de piel verde que posee una fuerza, resistencia y velocidad sobrehumanas, casi ilimitadas, además de poseer un factor de curación y regeneración sumamente eficientes. Estas características incrementan su poder a medida que su furia aumenta. Su alter ego es el doctor Bruce Banner un genio científico quien, debido a la exposición a la radiación gamma, se transforma en Hulk cuando su corazón late a una velocidad insalubre. Hulk es la forma adoptada por Bruce Banner cada vez que está estresado o enfurecido. Esto se debe a la exposición de radiación gamma en un experimento tratando de encontrar una manera de hacer a los seres humanos inmunes a dicha radiación.'
  },

  {
    id: 8,
    nombre: 'Black Widow',
    nombre_real: 'Natasha Romanova',
    imagen: 'assets/img/black_widow.jpg',
    imagen2: 'assets/img/black_widow2.jpg',
    descripcion: 'Natasha es descendiente de los zares rusos. Cuando era una niña fue encontrada por el soldado Ivan Petrovich, quien la cuidó hasta que llegó a la edad adulta. Natasha se convirtió en una famosa bailarina en la Unión Soviética y, más tarde, se casó con Alexi Shostakov. La KGB vio en la pareja una buena posibilidad de tener dos buenos agentes y, mientras Alexi estaba en un viaje de gobierno, se le informó de los planes que el régimen tenía para él. También se le prohibió tener cualquier tipo de contacto con conocidos, a los cuales se les dijo que este había muerto en una explosión. Tal como el gobierno había anticipado, Natasha quiso hacer algo para honrar la memoria de su esposo; por lo cual fue reclutada para convertirse en espía.'
  },

  {
    id: 9,
    nombre: 'Iron Man',
    nombre_real: 'Anthony Edward Stark',
    imagen: 'assets/img/iron_man.jpg',
    imagen2: 'assets/img/iron_man2.jpg',
    descripcion: 'Stark es un exitoso multimillonario, empresario e ingeniero, con una lujosa vida y una enorme fortuna gracias a sus inventos y a la herencia de su padre. Su vida toma un giro repentino cuando es secuestrado por un grupo terrorista y sufre una grave lesión en el pecho cuando un fragmento de metralla se clava cerca de su corazón y amenaza con matarlo. Sus captores tratan de obligarlo a construir un arma de destrucción masiva para ellos. Él crea en su lugar una poderosa armadura de hierro con armas incorporadas en el que él se meterá para salvar su vida y escapar de su cautiverio.'
  },

  {
    id: 10,
    nombre: 'Cyclops',
    nombre_real: 'Scott Summers',
    imagen: 'assets/img/cyclops.jpg',
    imagen2: 'assets/img/cyclops2.jpg',
    descripcion: 'Scott Summers es el mayor de los hijos de Christopher y Katherina Anne. Cuando Scott era todavía un niño su padre llevó a la familia a un viaje en su avión particular, en medio del vuelo fueron atacados por una nave Shi\'ar. Christopher impidió que capturasen a sus hijos lanzándolos en el único paracaídas del avión. Durante la caída el paracaídas se incendió y se hubieran matado si Scott no hubiese lanzado un rayo óptico de forma inconsciente que amortiguó el golpe, aunque Scott se golpeó la cabeza duramente, lo que provocó su incapacidad para controlar sus poderes en el futuro.'
  },

   {
    id: 11,
    nombre: 'Captain Marvel',
    nombre_real: 'Carol Danvers',
    imagen: 'assets/img/captain_marvel.jpg',
    imagen2: 'assets/img/captain_marvel2.jpg',
    descripcion: 'Carol Susan Jane Danvers comenzó su carrera en la fuerza aérea de Estados Unidos y llegó hasta el puesto de jefe de seguridad de Cabo Cañaveral. Allí se vio relacionada con el Capitán Marvel, un soldado kree que desertaría de sus funciones para proteger a la Tierra de su propio mundo. Una pelea entre Marvel y el comandante Yon-Rogg terminó con la explosión de un arma kree, que afectó a Carol. La radiación modificó su estructura genética y la convirtió en un híbrido humano-kree de enormes poderes. Entonces asumió la identidad de Ms. Marvel, con un traje similar al del propio Capitán Marvel, que pronto cambió por uno negro. Trabajó varias veces con los Vengadores y finalmente se unió a ellos cuando la Bruja Escarlata se ausentó.'
  },

  {
   id: 12,
   nombre: 'Storm',
   nombre_real: 'Ororo Munroe',
   imagen: 'assets/img/storm.jpg',
   imagen2: 'assets/img/storm2.jpg',
   descripcion: 'Es la hija de David Munroe, un reportero gráfico norteamericano, y de N\'Dare, la princesa de una tribu africana de Kenia. A la edad de seis meses se fueron a vivir a El Cairo, ya que su padre trabajaba como corresponsal en esa ciudad. Cinco años más tarde estalló una guerra civil en Egipto, y a causa de uno de los bombardeos en la ciudad, la casa donde vivían quedó destruida con ellos dentro. Ororo presenció la muerte de sus padres atrapada entre las ruinas y aquella experiencia le provocó un profundo trauma, a raíz del cual desarrolló una fuerte claustrofobia.'
 },

 {
  id: 13,
  nombre: 'Doctor Strange',
  nombre_real: 'Stephen Vincent Strange',
  imagen: 'assets/img/doctor_strange.jpg',
  imagen2: 'assets/img/doctor_strange2.jpg',
  descripcion: 'Stephen Strange es un médico especializado en neurocirugía, codicioso y egocéntrico, hasta que en un accidente sufrió una enfermedad nerviosa en sus manos que le obligó a retirarse. Cuando su padre murió, su hermano fue a visitarlo para recriminarle el no haber ido al funeral ese día. Stephen estaba con una chica, por lo que su hermano salió enfadado. Estaba nevando esa noche y hubo un accidente en el cual su hermano murió al ser atropellado. Stephen hizo que su cadáver fuera criogenizado hasta el día en que la ciencia lo pudiera revivir.'
},

{
 id: 14,
 nombre: 'Black Panther',
 nombre_real: 'T\'Challa',
 imagen: 'assets/img/black_panther.jpg',
 imagen2: 'assets/img/black_panther2.png',
 descripcion: 'Sigilosa criatura de la noche, Black Panther manipula eventos globales con una facilidad escalofriante y una frialdad aparente, para disgusto y confusión de sus aliados superheroicos. Debido a sus responsabilidades de Estado, T\'Challa se ha negado los inmensos placeres que su enorme fortuna podría darle y su felicidad con el amor de su vida, Ororo Munroe (Storm), miembro de los X-Men. Nada es suficiente para el enigmático rey cuando se trata de proteger a su país.'
},

{
 id: 15,
 nombre: 'Luke Cage',
 nombre_real: 'Carl Lucas',
 imagen: 'assets/img/luke_cage.jpg',
 imagen2: 'assets/img/luke_cage2.jpg',
 descripcion: 'Nació en Georgia y creció cometiendo pequeños hurtos con su amigo Willis Stryker. A medida que los dos crecían, Lucas encontró trabajos ocasionales en diferentes empleos legales, mientras que Stryker se convertía en un delincuente profesional, especializado en vender "protección" a diversos establecimientos. Los dos amigos pretendían a la misma mujer, Reva Connors, pero la riqueza ilícita de Stryker le convertía en el pretendiente con más posibilidades. Cuando Stryker fue gravemente herido tras el ataque de una banda rival, Lucas le salvó la vida. Mientras Stryker estaba hospitalizado, Lucas y Connors se vieron con asiduidad, y entre ellos se desarrolló una profunda amistad. Loco de celos, Stryker puso dos kilos de heroína en casa de Lucas, delatándole luego a la policía, que lo arrestó y lo encarceló. Cuando estaba en prisión, Reva Connors murió en un ataque contra Stryker. Lucas juró vengarse de su antiguo compañero.'
}

]

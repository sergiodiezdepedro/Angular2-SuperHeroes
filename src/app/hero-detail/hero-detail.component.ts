import 'rxjs/add/operator/switchMap';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

import { Heroe } from '../heroe';
import { HeroeService }  from '../heroe.service';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.css']
})
export class HeroDetailComponent implements OnInit {
  heroe : Heroe;
  constructor(
    private heroeService: HeroeService,
    private route: ActivatedRoute,
    private location: Location
  ) {}

 ngOnInit(): void {
    this.route.params
      .switchMap((params: Params) => this.heroeService.getHeroe(+params['id']))
      .subscribe(heroe => this.heroe = heroe);
  }

  goBack(): void {
    this.location.back();
  }

}

import { Injectable } from '@angular/core';
import { Heroe } from './heroe';
import { HEROES } from './data-heroes'

@Injectable()
export class HeroeService {

  getHeroes(): Promise<Heroe[]>{
    return Promise.resolve(HEROES);
  }

  getHeroe(id: number): Promise<Heroe> {
    return this.getHeroes()
               .then(heroes => heroes.find(heroe => heroe.id === id));
  }

  constructor() {}

}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Heroe } from '../heroe';
import { HeroeService } from '../heroe.service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css'],
})
export class HeroesComponent implements OnInit {
  heroes : Heroe[];
  selectedHeroe: Heroe;

  constructor(
    private router: Router,
    private heroeService: HeroeService) { }

  getHeroes(): void {
    this.heroeService.getHeroes().then(heroes => this.heroes = heroes);
  }

  ngOnInit(): void {
    this.getHeroes();
  }

  onSelect(heroe: Heroe): void {
    this.selectedHeroe = heroe;
  }

  gotoDetail(): void {
    this.router.navigate(['/detail', this.selectedHeroe.id]);
  }
  
}
